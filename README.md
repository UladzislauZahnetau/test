# testProject



## Getting started


- Install node_modules
```
npm i
```
- Install pods if ios
```
bundle install
npx pod-install ios
```
- Run android
```
npx react-native run-android
```
- Run ios
```
npx react-native run-ios
```
- lint
```
npm run lint
```

- Screenshots Android

```
https://drive.google.com/drive/folders/1ZkUBIllMmKEVYJPdrVBj2vdDwx2pV_Gn?usp=sharing
```

- Screenshots IOS

```
https://drive.google.com/drive/folders/1ajQPZngmQK7DRNot33NZSLeD8zf28rlb?usp=sharing
```
