module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.js', '.json', '.ios.jsx', '.android.jsx', '.jsx'],
        root: ['.'],
        alias: {
          '@': './src',
        },
      },
    ],
  ],
};
