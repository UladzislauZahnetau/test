import {StyleSheet} from 'react-native';

import {colors, offsets} from '@/constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: colors.secondary.dark,
    borderTopLeftRadius: offsets.large,
    borderTopRightRadius: offsets.large,
  },
  button: {
    position: 'absolute',
    bottom: offsets.large,
    right: offsets.large,
  },
});
