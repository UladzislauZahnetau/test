import React from 'react';
import {View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {v4 as uuidv4} from 'uuid';

import {AddTipForm} from '@/forms';
import {Button} from '@/components';
import {IconCross} from '@/assets';
import {add} from '@/reducers';
import {selectUser} from '@/selectors';

import {styles} from './styles';

export const AddTip = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const user = useSelector(selectUser);

  const handleSubmit = values => {
    const payload = {
      id: uuidv4(),
      avatar: user.avatar,
      from: user.username,
      to: values.to,
      amount: Number(values.amount),
      date: new Date().toUTCString(),
      message: values.message,
    };
    dispatch(add(payload));
    navigation.goBack();
  };

  const handleClose = () => {
    navigation.goBack();
  };

  const initialValues = {
    to: '',
    amount: '',
    message: '',
  };

  return (
    <View style={styles.container}>
      <AddTipForm onSubmit={handleSubmit} initialValues={initialValues} />
      <Button.Icon
        ButtonIcon={IconCross}
        containerStyle={styles.button}
        onPress={handleClose}
      />
    </View>
  );
};
