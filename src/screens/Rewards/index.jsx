import React from 'react';
import {View} from 'react-native';
import {useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/native';

import {List} from '@/components';
import {selectMyRewards, selectRewards} from '@/selectors';

import {styles} from './styles';
import {screens} from '@/constants';

export const Rewards = () => {
  const {name} = useRoute();

  const rewards = useSelector(
    name === screens.ROUTE_MY_REWARDS ? selectMyRewards : selectRewards,
  );

  return (
    <View style={styles.container}>
      <List data={rewards} />
    </View>
  );
};
