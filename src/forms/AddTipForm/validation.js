import * as Yup from 'yup';

export const validationSchema = Yup.object({
  to: Yup.string().required('Field to is required'),
  amount: Yup.string().required('Field amount is required'),
  message: Yup.string().required('Field message is required'),
});
