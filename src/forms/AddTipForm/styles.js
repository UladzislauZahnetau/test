import {StyleSheet} from 'react-native';

import {colors, offsets} from '@/constants';

export const styles = StyleSheet.create({
  header: {
    marginTop: offsets.large,
    alignSelf: 'center',
    color: colors.primary.light,
  },
  content: {
    margin: offsets.large,
  },
  button: {
    width: '80%',
    alignSelf: 'center',
    backgroundColor: colors.primary.light,
    marginTop: offsets.medium,
  },
  buttonLabel: {
    color: colors.secondary.dark,
  },
});
