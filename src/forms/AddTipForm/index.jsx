import React from 'react';
import {View} from 'react-native';
import {useFormik} from 'formik';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';

import {Typography, Field, Button} from '@/components';

import {validationSchema} from './validation';
import {styles} from './styles';

const EXTRA_HEIGHT = 120;

export const AddTipForm = ({initialValues, onSubmit}) => {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
  });

  const {handleChange, handleBlur, values, errors, handleSubmit} = formik;

  return (
    <KeyboardAwareScrollView enableOnAndroid extraHeight={EXTRA_HEIGHT}>
      <Typography.H1 text="Give Reward" style={styles.header} />
      <View style={styles.content}>
        <Field
          label="To"
          onChangeText={handleChange('to')}
          onBlur={handleBlur('to')}
          value={values.to}
          error={errors.to}
        />
        <Field
          keyboardType="decimal-pad"
          label="Amount"
          onChangeText={handleChange('amount')}
          onBlur={handleBlur('amount')}
          value={values.amount}
          error={errors.amount}
        />
        <Field
          label="Message"
          multiline
          numberOfLines={4}
          onChangeText={handleChange('message')}
          onBlur={handleBlur('message')}
          value={values.message}
          error={errors.message}
        />
        <Button.Default
          label="Give"
          onPress={handleSubmit}
          containerStyle={styles.button}
          labelStyle={styles.buttonLabel}
        />
      </View>
    </KeyboardAwareScrollView>
  );
};
