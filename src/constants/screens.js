const ROUTE_REWARDS = 'Rewards';
const ROUTE_FEED = 'Feed';
const ROUTE_MY_REWARDS = 'My Rewards';
const ROUTE_ADD_TIP = 'Add Tip';

export const screens = {
  ROUTE_REWARDS,
  ROUTE_FEED,
  ROUTE_MY_REWARDS,
  ROUTE_ADD_TIP,
};
