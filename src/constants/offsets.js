export const offsets = {
  small: 6,
  medium: 16,
  large: 20,
  monster: 32,
};
