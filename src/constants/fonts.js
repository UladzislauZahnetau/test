import {Platform} from 'react-native';

export const fontFamilies = {
  thin: Platform.OS === 'ios' ? 'ProximaNova-Thin' : 'Proxima_Nova_Thin',
  light: Platform.OS === 'ios' ? 'ProximaNova-Light' : 'Proxima_Nova_Light',
  regular:
    Platform.OS === 'ios' ? 'ProximaNova-Regular' : 'Proxima_Nova_Regular',
  semibold:
    Platform.OS === 'ios' ? 'ProximaNova-Semibold' : 'Proxima_Nova_Semibold',
  bold: Platform.OS === 'ios' ? 'ProximaNova-Bold' : 'Proxima_Nova_Bold',
};

export const fontSizes = {
  small: 10,
  middle: 14,
  large: 18,
  monster: 20,
};
