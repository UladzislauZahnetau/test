export const colors = {
  primary: {
    light: '#ffffff',
    dark: '#f5f1ee',
  },
  secondary: {
    light: '#e4e2df',
    medium: 'gray',
    dark: '#000000',
  },
  tertiary: {
    light: '#b39e8a',
    dark: '#998673',
  },
  error: '#e74c4c',
};
