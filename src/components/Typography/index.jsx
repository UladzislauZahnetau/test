import React from 'react';
import {Text} from 'react-native';

import {styles} from './styles';

export const Default = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.default, style]}>
      {text}
    </Text>
  );
};

export const H1 = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.h1, style]}>
      {text}
    </Text>
  );
};

export const H2 = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.h2, style]}>
      {text}
    </Text>
  );
};

export const H3 = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.h3, style]}>
      {text}
    </Text>
  );
};

export const Label = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.label, style]}>
      {text}
    </Text>
  );
};

export const Error = ({text, style, ...props}) => {
  return (
    <Text {...props} style={[styles.error, style]}>
      {text}
    </Text>
  );
};

export const Typography = {
  Default,
  H1,
  H2,
  H3,
  Label,
  Error,
};
