import {StyleSheet} from 'react-native';

import {fontFamilies, colors, fontSizes, offsets} from '@/constants';

export const styles = StyleSheet.create({
  default: {
    fontFamily: fontFamilies.semibold,
    fontSize: fontSizes.small,
    color: colors.secondary.medium,
  },
  h1: {
    fontFamily: fontFamilies.bold,
    color: colors.secondary.dark,
    fontSize: fontSizes.monster,
  },
  h2: {
    fontFamily: fontFamilies.bold,
    color: colors.secondary.dark,
    fontSize: fontSizes.large,
  },
  h3: {
    fontFamily: fontFamilies.semibold,
    color: colors.secondary.dark,
    fontSize: fontSizes.middle,
  },
  label: {
    fontFamily: fontFamilies.regular,
    color: colors.tertiary.dark,
    marginLeft: offsets.small,
    marginBottom: offsets.small,
  },
  error: {
    fontFamily: fontFamilies.regular,
    color: colors.error,
  },
});
