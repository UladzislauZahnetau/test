import React from 'react';
import {FlatList, View} from 'react-native';

import {Avatar, Typography} from '@/components';
import {formatDate} from '@/helpers';

import {styles} from './styles';

const Item = ({item}) => {
  const {id, avatar, message, from, to, date} = item;

  const rewardedBy = `${to} Rewarded by ${from}`;
  const rewardDate = formatDate(date);

  return (
    <View key={id} style={styles.itemContainer}>
      <Avatar.Small source={avatar} />
      <View key={item.id} style={styles.itemContent}>
        <Typography.H3 text={message} />
        <Typography.Default text={rewardedBy} />
        <Typography.Default text={rewardDate} />
      </View>
    </View>
  );
};

export const List = ({data}) => (
  <FlatList
    data={data}
    renderItem={Item}
    style={styles.container}
    showsVerticalScrollIndicator={false}
  />
);
