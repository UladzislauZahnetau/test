import {StyleSheet} from 'react-native';

import {colors, offsets} from '@/constants';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary.light,
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    margin: offsets.large,
  },
  itemContent: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: offsets.large,
  },
});
