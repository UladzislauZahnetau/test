import {StyleSheet} from 'react-native';

import {colors} from '@/constants';

const INPUT_HEIGHT = 50;

export const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: colors.tertiary.dark,
    color: colors.primary.light,
    height: INPUT_HEIGHT,
  },
});
