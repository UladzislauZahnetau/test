import React from 'react';
import {TextInput} from 'react-native';

import {styles} from './styles';

const Default = ({...props}) => {
  return <TextInput {...props} style={styles.input} />;
};

export const Input = {Default};
