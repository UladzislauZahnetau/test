import {offsets} from '@/constants';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    position: 'relative',
    paddingBottom: offsets.large,
  },
  error: {
    position: 'absolute',
    bottom: 0,
    left: offsets.small,
  },
});
