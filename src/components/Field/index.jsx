import React from 'react';
import {View} from 'react-native';

import {Input, Typography} from '@/components';

import {styles} from './styles';

export const Field = ({label, error, ...props}) => {
  return (
    <View style={styles.container}>
      <Typography.Label text={label} />
      <Input.Default {...props} />
      <Typography.Error text={error} style={styles.error} />
    </View>
  );
};
