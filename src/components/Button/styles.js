import {StyleSheet} from 'react-native';

import {colors} from '@/constants';

const BUTTON_WIDTH = 60;
const BUTTON_HEIGHT = 40;

export const styles = StyleSheet.create({
  button: {
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    borderRadius: BUTTON_HEIGHT / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.secondary.dark,
  },
});
