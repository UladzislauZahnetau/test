import React from 'react';
import {TouchableOpacity} from 'react-native';

import {Typography} from '@/components';
import {colors} from '@/constants';

import {styles} from './styles';

const Default = ({label, containerStyle, labelStyle, ...props}) => {
  return (
    <TouchableOpacity {...props} style={[styles.button, containerStyle]}>
      <Typography.H1 text={label} style={labelStyle} />
    </TouchableOpacity>
  );
};

const Icon = ({ButtonIcon, containerStyle, ...props}) => {
  return (
    <TouchableOpacity {...props} style={[styles.button, containerStyle]}>
      <ButtonIcon fill={colors.primary.light} />
    </TouchableOpacity>
  );
};

export const Button = {Default, Icon};
