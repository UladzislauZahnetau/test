import {StyleSheet} from 'react-native';

const AVATAR_REGULAR = 100;
const AVATAR_SMALL = 50;

export const styles = StyleSheet.create({
  regular: {
    width: AVATAR_REGULAR,
    height: AVATAR_REGULAR,
    borderRadius: AVATAR_REGULAR / 2,
  },
  small: {
    width: AVATAR_SMALL,
    height: AVATAR_SMALL,
    borderRadius: AVATAR_SMALL / 2,
  },
});
