import React from 'react';
import {Image} from 'react-native';

import {styles} from './styles';

const Default = props => (
  <Image {...props} style={[styles.default, styles.regular]} />
);

const Small = props => (
  <Image {...props} style={[styles.default, styles.small]} />
);

export const Avatar = {Default, Small};
