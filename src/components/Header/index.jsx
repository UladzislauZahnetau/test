import React from 'react';
import {View, Text} from 'react-native';
import {useSelector} from 'react-redux';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Avatar, Typography} from '@/components';
import {selectUser, selectGivenAmount, selectReceivedAmount} from '@/selectors';

import {styles} from './styles';
import {offsets} from '@/constants';

export const Header = () => {
  const {avatar, username} = useSelector(selectUser);
  const givenAmount = useSelector(selectGivenAmount);
  const receivedAmount = useSelector(selectReceivedAmount);
  const {top} = useSafeAreaInsets();

  return (
    <View style={[styles.container, {paddingTop: top + offsets.large}]}>
      <Avatar.Default source={avatar} />
      <View style={styles.content}>
        <Typography.H2 text={username} />
        <Text>
          <Typography.H3 text="Given " />
          <Typography.H1 text={`$${givenAmount}`} style={styles.amount} />
          <Typography.H3 text=" / Received " />
          <Typography.H1 text={`$${receivedAmount}`} style={styles.amount} />
        </Text>
      </View>
    </View>
  );
};
