import {StyleSheet} from 'react-native';

import {colors, fontSizes, offsets} from '@/constants';

export const styles = StyleSheet.create({
  container: {
    padding: offsets.large,
    flexDirection: 'row',
    backgroundColor: colors.primary.dark,
  },
  content: {
    marginLeft: offsets.large,
    justifyContent: 'center',
  },
  amount: {
    fontSize: fontSizes.middle,
  },
});
