import {createSlice} from '@reduxjs/toolkit';
import {v4 as uuidv4} from 'uuid';

import {AVATAR_JESSE_PINKMAN, AVATAR_WALTER_WHITE} from '@/assets';
import {slices} from '@/constants';

const initialState = {
  rewards: [
    {
      id: uuidv4(),
      avatar: AVATAR_WALTER_WHITE,
      from: 'Walter White',
      to: 'Jesse Pinkman',
      amount: 50,
      date: new Date().toUTCString(),
      message: 'Big thanks for your help!',
    },
    {
      id: uuidv4(),
      avatar: AVATAR_WALTER_WHITE,
      from: 'Walter White',
      to: 'Jesse Pinkman',
      amount: 100,
      date: new Date().toUTCString(),
      message: 'Big thanks for your help!',
    },
    {
      id: uuidv4(),
      avatar: AVATAR_JESSE_PINKMAN,
      from: 'Jesse Pinkman',
      to: 'Walter White',
      amount: 100,
      date: new Date().toUTCString(),
      message: 'Big thanks for your help!',
    },
  ],
};

export const rewardsSlice = createSlice({
  initialState,
  name: slices.rewards,
  reducers: {
    add: (state, action) => {
      return {...state, rewards: [...state.rewards, action.payload]};
    },
  },
});

export const {add} = rewardsSlice.actions;

export const rewardsReducer = rewardsSlice.reducer;
