import {createSlice} from '@reduxjs/toolkit';

import {AVATAR_JESSE_PINKMAN} from '@/assets';
import {slices} from '@/constants';

const initialState = {
  avatar: AVATAR_JESSE_PINKMAN,
  username: 'Jesse Pinkman',
};

export const userSlice = createSlice({
  initialState,
  name: slices.user,
});

export const userReducer = userSlice.reducer;
