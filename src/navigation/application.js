import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {screens} from '@/constants';
import {AddTip} from '@/screens';

import {RewardsNavigation} from './rewards';

const Stack = createStackNavigator();

const navigationOptions = {
  headerShown: false,
};

const modalNavigationOptions = {
  presentation: 'transparentModal',
};

const applicationRoutes = {
  [screens.ROUTE_REWARDS]: {
    component: RewardsNavigation,
  },
  [screens.ROUTE_ADD_TIP]: {
    component: AddTip,
    options: modalNavigationOptions,
  },
};

export const ApplicationtNavigation = () => (
  <Stack.Navigator
    screenOptions={navigationOptions}
    initialRouteName={screens.ROUTE_REWARDS}>
    {Object.entries(applicationRoutes).map(([name, {...props}], key) => (
      <Stack.Screen key={key} name={name} {...props} />
    ))}
  </Stack.Navigator>
);
