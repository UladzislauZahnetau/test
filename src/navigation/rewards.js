import React from 'react';
import {StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useNavigation} from '@react-navigation/native';

import {screens, fontSizes, fontFamilies, offsets, colors} from '@/constants';
import {Rewards} from '@/screens';
import {Button} from '@/components';
import {IconAdd} from '@/assets';

const styles = StyleSheet.create({
  tabBarLabelStyle: {
    fontSize: fontSizes.middle,
    textTransform: 'capitalize',
    fontFamily: fontFamilies.bold,
  },
  tabBarStyle: {
    width: '100%',
    overflow: 'hidden',
    borderTopLeftRadius: offsets.large,
    borderTopRightRadius: offsets.large,
    backgroundColor: colors.secondary.light,
    elevation: 0,
  },
  tabBarIndicatorStyle: {
    height: '100%',
    backgroundColor: colors.primary.light,
  },
  button: {
    position: 'absolute',
    bottom: offsets.large,
    right: offsets.large,
  },
});

const navigationOptions = {
  headerShown: false,
  tabBarLabelStyle: styles.tabBarLabelStyle,
  tabBarStyle: styles.tabBarStyle,
  tabBarActiveTintColor: colors.tertiary.light,
  tabBarInactiveTintColor: colors.secondary.dark,
  tabBarPressColor: colors.primary.light,
  tabBarIndicatorStyle: styles.tabBarIndicatorStyle,
};

const rewardsRoutes = {
  [screens.ROUTE_FEED]: {
    component: Rewards,
  },
  [screens.ROUTE_MY_REWARDS]: {
    component: Rewards,
  },
};

const Tab = createMaterialTopTabNavigator();

export const RewardsNavigation = () => {
  const navigation = useNavigation();

  const handleAddTip = () => {
    navigation.navigate(screens.ROUTE_ADD_TIP);
  };

  return (
    <React.Fragment>
      <Tab.Navigator screenOptions={navigationOptions}>
        {Object.entries(rewardsRoutes).map(([name, {...props}], key) => (
          <Tab.Screen key={key} name={name} {...props} />
        ))}
      </Tab.Navigator>
      <Button.Icon
        ButtonIcon={IconAdd}
        containerStyle={styles.button}
        onPress={handleAddTip}
      />
    </React.Fragment>
  );
};
