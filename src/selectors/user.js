import {slices} from '@/constants';

export const selectUser = state => state[slices.user];
