import {slices} from '@/constants';

export const selectRewards = state => state[slices.rewards].rewards;

export const selectMyRewards = state =>
  state[slices.rewards].rewards.filter(
    item => item.to === state[slices.user].username,
  );

export const selectGivenAmount = state =>
  state[slices.rewards].rewards.reduce(
    (prev, curr) =>
      curr.from === state[slices.user].username ? curr.amount + prev : prev,
    0,
  );

export const selectReceivedAmount = state =>
  state[slices.rewards].rewards.reduce(
    (prev, curr) =>
      curr.to === state[slices.user].username ? curr.amount + prev : prev,
    0,
  );
