import AVATAR_JESSE_PINKMAN from './images/Jesse_Pinkman.jpg';
import AVATAR_WALTER_WHITE from './images/Walter_White.jpg';
import IconAdd from './icons/add.svg';
import IconCross from './icons/cross.svg';

export {AVATAR_JESSE_PINKMAN, AVATAR_WALTER_WHITE, IconAdd, IconCross};
