import {configureStore, combineReducers} from '@reduxjs/toolkit';

import {rewardsReducer, userReducer} from '@/reducers';
import {slices} from '@/constants';

const reducers = combineReducers({
  [slices.user]: userReducer,
  [slices.rewards]: rewardsReducer,
});

export const store = configureStore({
  reducer: reducers,
});
